package com.tenio.b142.s02.s02app.services;

import com.tenio.b142.s02.s02app.models.Post;
import com.tenio.b142.s02.s02app.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImplementation implements PostService{
    @Autowired
    private PostRepository postRepositories;

    public void createPost(Post newPost) {
        postRepositories.save(newPost);

    }

    public void updatePost(Long id, Post updatedPost) {
        Post existingPost = postRepositories.findById(id).get();
        existingPost.setTitle(updatedPost.getTitle());
        existingPost.setContent(updatedPost.getContent());
        postRepositories.save(existingPost);

    }

    public void deletePost(Long id) {
        postRepositories.deleteById(id);
    }

    public Iterable<Post> getPosts() {
        return postRepositories.findAll();
    }

}
